# Acces and Evasion
This lecture will discuss the fundamental concepts of cyber defense and how a well-designed defensive architecture can be tuned to detect/hunt for specific threats. We will then take those fundamentals and apply them, both defensively to provide insight into the spread of a threat in a network, and offensively to evade existing detections to provide continued access into a simulated target.

Topics include:
* Threats
* Defense Strategies
* Defense Tools
  * Antivirus
  * Firewalls
  * NID/NIPS
  * HIDS/EDR
  * SIEMs

## Range Information

### DCO Access Info:
- Security VM: ssh ubuntu@(RANGE-IP) -p 23
  - Password: ubuntu
- Victim VM: RDP to Administrator on (RANGE-IP)
  - Password: Administrator
  - ex. rdesktop -u Administrator -p Administrator \[RANGE-IP\]:3389
- Router VM: ssh vyos@(RANGE-IP)
  - Password: vyos

### IP Allocations
| Team          | Range IP                    | Windows VM  | DCO VM|
|:---           |   :---:                     | :---:       | ---:  |
| Team 0        | 192.168.11.100              | 10.61.100.5 |10.61.101.5|
| Team 1        | 192.168.11.101              | 10.61.102.5 |10.61.103.5|
| Team 2        | 192.168.11.102              | 10.61.104.5 |10.61.105.5|
| Team 3        | 192.168.11.103              | 10.61.106.5 |10.61.107.5|
| Team 4        | 192.168.11.104              | 10.61.107.5 |10.61.108.5|
| Team 5        | 192.168.11.105              | 10.61.109.5 |10.61.110.5|
| Team 6        | 192.168.11.106              | 10.61.111.5 |10.61.112.5|
| Team 7        | 192.168.11.107              | 10.61.113.5 |10.61.114.5|
| Team 8        | 192.168.11.108              | 10.61.115.5 |10.61.116.5|
| Team 9        | 192.168.11.109              | 10.61.117.5 |10.61.118.5|



![Lab Network Map](network_v3_irt.png)